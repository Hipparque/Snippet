import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;


public class FixedStreamThreads {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        int nbThreads = 2;
        if (args.length > 0) {
            nbThreads = Integer.parseInt(args[0]);
        }

        Instant start = Instant.now();
        ForkJoinPool forkJoinPool = new ForkJoinPool(nbThreads);
        double value = forkJoinPool.submit(()->
                IntStream.range(1, 2_000_000_000).parallel().filter(n->(n%2==0)).mapToDouble(n->Math.log(n)).sum()
                ).get();
        System.out.printf("sum(ln(nb_pair)) = %f en %ds avec %d threads%n", value, Duration.between(start, Instant.now()).getSeconds(), nbThreads);
    }
}

