#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np

from pandas import DataFrame


class DecoratorPatternExample:
    """Decorator class.

    An example of the decorator pattern in python using operator
    overloading.
    """

    def __init__(self, df: DataFrame):
        """Constructor."""
        self._df = df

        #  Instead of overloading manually all operators, we could do something
        #  like this:
        #  tmp = filter(
        #      lambda n: n.startswith('__') and n.endswith('__'),
        #      dir(self._df)
        #  )
        #  for attr in tmp:
        #      setattr(self, attr, getattr(self._df, attr))

    def filter(self, mask):
        """Apply the given mask to the dataset."""
        self._df = self._df[mask]

        #  Because I like return the object and have a more streamline
        #  interface.
        return self

    def __repr__(self):
        """Representation of the object.

        This method is used when you just type the object in a python console.
        """
        return repr(self._df)

    def __str__(self):
        """Convert to string.

        This method return a string represnetation of the object. Ie it convert
        it to string. This method is used when you are converting it into
        string.
        """
        return str(self._df)

    def __getattr__(self, name):
        """Overload getattr.

        This method is the of the pattern key as it make sure you can access
        all method from your object and the decorated one seamlessly.

        Given how DataFrame works, we should alsoj overload (g|s)etitem to
        reproduce the dict-like behavior.
        """
        if name in dir(self):
            return getattr(self, name)

        if name in dir(self._df):
            def convert(*args, **kwargs):
                """Make sure that DataFrame are redecorated."""
                res = getattr(self._df, name)(*args, **kwargs)
                if isinstance(res, DataFrame):
                    return DecoratorPatternExample(res)
                return res
            return convert

        raise AttributeError("Attribute '%s' does not exist." % name)

    def __setitem__(self, key, value):
        """Set the given name to the given value."""
        self._df[key] = value

    def __getitem__(self, key):
        """Set the given name to the given value."""
        if isinstance(self._df[key], DataFrame):
            return DecoratorPatternExample(self._df[key])
        return self._df[key]

    def __len__(self):
        """Override 'len'."""
        return len(self._df)

    def __lt__(self, value):
        """Operator overloading '<'."""
        return self._df.__lt__(value)

    def __le__(self, value):
        """Operator overloading '<='."""
        return self._df.__le__(value)

    def __ge__(self, value):
        """Operator overloading '>'."""
        return self._df.__ge__(value)

    def __gt__(self, value):
        """Operator overloading '>='."""
        return self._df.__gt__(value)
